/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 *
 * @author asus
 */
public class ColTrans {

    static String decrypt(String value, int key) { 
         //add space to buffer to get full rectangle
        while (value.length()%key!=0){
            value+=" ";           
        }
       // Convert to char array.
	char[] buffer = value.toCharArray();
        int arrayLength=buffer.length;
        int row=arrayLength/key;
         String trans="";
        int count=0;          
	// Loop over characters.
	for (int i = 0;i<arrayLength; i++) {
            if (count>=arrayLength) count=count-arrayLength+1;         
            trans+=buffer[count];                  
            count+=row;          
        }
// Return final string.
	return trans;
    }  
    
     static String encrypt(String value, int key) {  
        //add space to buffer to get full rectangle
        while (value.length()%key!=0){
            value+=" ";           
        }
        // Convert to char array.
	char[] buffer = value.toCharArray();
        int arrayLength=buffer.length;
        String trans="";
        int count=0;          
	// Loop over characters.
	for (int i = 0;i<arrayLength; i++) {
            if (count>=arrayLength) count=count-arrayLength+1;    
            trans+=buffer[count];                  
            count+= key;   
        }
// Return final string.
	return trans;
    }  
    /**
     * @param args the command line arguments
     */
   
public static String readTextFile(String fileName) throws IOException {
    String content = new String(Files.readAllBytes(Paths.get(fileName)));
    content=content.substring(1, content.length()-2);
    return content;
  }
  
  public static void writeToTextFile(String fileName, String content) throws IOException {
   content=">".concat(content).concat("<");   
    Files.write(Paths.get(fileName), content.getBytes(), StandardOpenOption.CREATE);
  }
  
    public static void main(String[] args) throws IOException{
    String input = readTextFile(args[1]);        
    String command=args[0];
    switch (command){
    case "d":
        String plaintext=decrypt(input,Integer.parseInt(args[2]));
        System.out.println(plaintext);
        writeToTextFile("plaintext.txt",plaintext);   
        break;
    case "e":        
        String ciphertext=encrypt(input,Integer.parseInt(args[2]));
        System.out.println(ciphertext);
        writeToTextFile("ciphertext.enc",ciphertext);   
        break;
    }
   }
    
}
