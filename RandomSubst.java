/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author asus
 */
public class Subst {
    
    static final char [] ALPHABET={'A','B','C','D','E','F','G','H','I','J','K','L','M',
              'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
              ' ','.',',',':',';','(',')','-','!','?','$','\'','"','\n',
              '0','1','2','3','4','5','6','7','8','9'};        
    static final int alphabetLength=ALPHABET.length;

    static String decrypt(String value, String key) { 
        //cut < > and new line of string
     //   value=value.substring(1, value.length()-2);
       // Convert to char array.
       value=value.toUpperCase();
       char[] buffer = value.toCharArray();
        int bufferLength=buffer.length;
        char[] keyArray = key.toCharArray();
        int keyLength=keyArray.length;
        String msg="";
            for (int i = 0; i <bufferLength; i++) {
                for (int j = 0; j < keyLength; j++) {
                    if((buffer[i]==keyArray[j])){
                     msg=msg+ALPHABET[j];
                    break;
                    }
               }
             }
// Return final string.
            return msg;
    }  
    
     static String encrypt(String value, String key) {
       //  value=value.substring(1, value.length()-2);  
        // Convert to char array.
       value=value.toUpperCase();
        char[] buffer = value.toCharArray();
        int bufferLength=buffer.length;
        char[] keyArray = key.toCharArray();
        int keyLength=keyArray.length;
        String msg="";
            for (int i = 0; i <bufferLength; i++) {
                for (int j = 0; j < alphabetLength; j++) {
                    if((buffer[i]==ALPHABET[j])){
                     msg=msg+keyArray[j];
                    break;
                    }
               }
             }
// Return final string.
            return msg;
    }  
     
     static void generate(String fileKeyName)throws IOException{// create array list object       
   List key = new ArrayList();
         for (int i = 0; i < alphabetLength; i++) {
             key.add(ALPHABET[i]);             
         }      
    // shuffle the list
   Collections.shuffle(key);
   String keyString = "";
for (int i = 0; i < alphabetLength; i++){
      keyString+=key.get(i);
}
    writeToTextFile(fileKeyName, keyString);        
     }
    /**
     * @param args the command line arguments
     */
   
public static String readTextFile(String fileName) throws IOException {
    String content = new String(Files.readAllBytes(Paths.get(fileName)));
    return content;
  }
  
  public static void writeToTextFile(String fileName, String content) throws IOException {
     Files.write(Paths.get(fileName), content.getBytes(), StandardOpenOption.CREATE);
  }
  
    public static void main(String[] args) throws IOException{
    String command=args[0];
    switch (command){
    case "d":
        String cipherTextInput = readTextFile(args[1]);     
        String keyDecrypt=readTextFile(args[2]);     
        String plaintext=decrypt(cipherTextInput,keyDecrypt);
        System.out.println(plaintext);
        writeToTextFile("plaintext.txt",plaintext);   
        break;
    case "e":
        String plainTextInput = readTextFile(args[1]);             
        String keyEncrypt=readTextFile(args[2]);     
        String ciphertext=encrypt(plainTextInput,keyEncrypt);
        System.out.println(ciphertext);
        writeToTextFile("ciphertext.enc",ciphertext);   
        break;
    case "g": generate(args[1]);
        
    }

}
}