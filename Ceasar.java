/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 *
 * @author asus
 */

public class Ceasar {
    static final char[] ALPHABET={
              'A','B','C','D','E','F','G','H','I','J','K','L','M',    
              'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',    
              ' ','.',',',':',';','(',')','-','!','?','$','\'','"',
              '\n',
              '0','1','2','3','4','5','6','7','8','9'};        
    static final int alphabetLength=ALPHABET.length;
    
 static String decrypt(String value, int key) {          
	// Convert to char array.
	char[] buffer = value.toCharArray();
	// Loop over characters.
	for (int i = 0; i < buffer.length; i++) {
	    // searching letter in the alpabet and Shift back with key 
	    char letter = buffer[i];
	           for (int j = 0; j < alphabetLength; j++) {                    
                if (letter==ALPHABET[j]) {
                    j-=key;                    
                    if(j<0)j+=50;
                    letter=ALPHABET[j];
                    break;
                }                          
            }
         buffer[i]=letter;  
         }
	// Return final string.
	return new String(buffer);
    }  
 
static String encrypt(String value, int key) {  
       // Convert to char array.
	char[] buffer = value.toCharArray();
	// Loop over characters.
	for (int i = 0; i < buffer.length; i++) {
	    // searching letter in the alpabet and Shift forward with key 
	    char letter = buffer[i];
	           for (int j = 0; j < alphabetLength; j++) {                    
                if (letter==ALPHABET[j]) {
                    j+=key;                    
                    if(j>=50)j-=50;
                    letter=ALPHABET[j];
                    break;
                }                          
            }
         buffer[i]=letter;  
         }
	// Return final string.
	return new String(buffer);
	
}
public static String readTextFile(String fileName) throws IOException {
    String content = new String(Files.readAllBytes(Paths.get(fileName)));
    content=content.substring(1, content.length()-2);
    return content;
  }
  
  public static void writeToTextFile(String fileName, String content) throws IOException {
   content=">".concat(content).concat("<\n");   
    Files.write(Paths.get(fileName), content.getBytes(), StandardOpenOption.CREATE);
  }
  
  
    public static void main(String[] args) throws IOException{
    String input = readTextFile(args[1]);
    String command=args[0];
    switch (command){
    case "d":              
        String plaintext=decrypt(input,Integer.parseInt(args[2]));
        System.out.println(plaintext);
        writeToTextFile("plaintext.txt",plaintext);   
        break;
    case "e":        
        String ciphertext=encrypt(input,Integer.parseInt(args[2]));
        System.out.println(ciphertext);
        writeToTextFile("ciphertext.enc",ciphertext);   
        break;
    }
   }
    
}
